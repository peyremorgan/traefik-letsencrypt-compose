# A Docker Compose file for Traefik

Adds auto-discovery, tracing and HTTPS to your Docker services, even without an orchestrator! Simply add this label to your Internet-facing services:

    services:
        my-service:
            ...
            networks:
                - frontend
            labels:
                - "traefik.frontend.rule=Host:example.com"
    networks:
        frontend:
            external:
                name: traefik
                
Replace example.com with the name of your virtual host (and, obviously, _my-service_ with the name of your service).
